//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by LineTracer.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_LineTracer_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_UPDATE_PARA          1000
#define IDC_EDIT_PARA                   1002
#define IDC_BUTTON_START                1003
#define IDC_BUTTON_STOP                 1004
#define IDC_CHECK_DISP                  1005
#define IDC_BUTTON1                     1006
#define IDC_BUTTON_TEST1                1006
#define IDC_BUTTON3                     1007
#define IDC_BUTTON_TEST2                1007
#define IDC_BUTTON_TEST3                1008
#define IDC_BUTTON_TEST4                1009
#define IDC_STATIC_TIME                 1010
#define IDC_SLIDER_LR                   1011
#define IDC_SLIDER_FB                   1012
#define IDC_RADIO_L                     1013
#define IDC_RADIO_C                     1014
#define IDC_RADIO_R                     1015
#define IDC_CHECK_AUTO_TUNING           1016
#define IDC_STATIC_PWM_L                1017
#define IDC_STATIC_PWM_R                1018
#define IDC_STATIC_VEL_L                1019
#define IDC_STATIC_VEL_R                1020
#define IDC_STATIC_MODE                 1021

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1022
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

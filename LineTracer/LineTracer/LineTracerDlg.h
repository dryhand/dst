
// LineTracerDlg.h : header file
//
#include "cv.h"
#include "cxcore.h"
#include "highgui.h"
#include "comm.h"
#include "afxcmn.h"

//#define SEARCH_LINE

#define CAM_WIDTH		640
#define CAM_HEIGHT		480

#pragma once

using namespace std;
using namespace cv;

typedef enum {
	M_ManualControl,
	M_TurnLeft,
	M_TurnRight,
	M_TurnBack,
	M_TrackingNormal,
	M_TrackingSlow,
#ifdef SEARCH_LINE
	M_SearchLine,
#endif
} Mode;

// CLineTracerDlg dialog
class CLineTracerDlg : public CDialogEx
{
// Construction
public:
	CLineTracerDlg(CWnd* pParent = NULL);	// standard constructor

	CWinThread	*m_pThreadCtrl;
	BOOL		m_bThreadCtrl;

	Mat			m_ImgIn;		// 카메라로부터의 입력 이미지
	Mat			m_ImgTemp;		// 임시 작업 이미지
	Mat			m_ImgOut;		// 출력 이미지

	BOOL		m_bCheck;		// 이미지 프로세싱 출력 확인용 
	CComm		m_Comm;			// 시리얼포트 통신 클래스

	Mode 		m_Mode;			// 현재 로봇의 동작 모드

	double		m_PassTime;
	int			m_PWM_Max;		// 최대 출력 PWM 값
	int			m_PWM_Avg;		// 평균 PWM 값
	int			m_PWM_L;		// 출력 PWM 값
	int			m_PWM_R;
	int			m_Man_L;		// Manual PWM 값
	int			m_Man_R;
	int			m_Vel_Avg;		// 평균 Vel 값
	int			m_Vel_L;		// 현재 로봇의 속도
	int			m_Vel_R;
	int			m_TurnCount;	// 로봇제어에 사용 (TurnRobot)

	int			m_Branch;
	BOOL		m_bAutoTuning;

	CSliderCtrl m_scFB;
	CSliderCtrl m_scLR;

	void	SendPacketRun(int Left, int Right);		// velocity mode : 속도제어를 한다.
	void	SendPacketOut(int Left, int Right);		// PWM mode : Openloop control
	void	SetTorque(int OnOff);					// 모터에 torque on/off
	void	ChangeMode(int Mode);					// Run <-> Out mode change
	void	GetVelocity(int *Left, int *Right);		// 양쪽 바퀴의 현재 속도를 얻는다. 

	int 	TurnRobot(int Count, int *PWM_L, int *PWM_R);
	void	ControlRobot(int RefPWM, double A, double B, int *PWM_L, int *PWM_R);
	int 	GetPWMAvg(int PWM);
	int 	GetVelAvg(int Vel);
	void	ThreadCtrl(void);

	// Dialog Data
	enum { IDD = IDD_LineTracer_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonStart();
	afx_msg void OnBnClickedButtonStop();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtonUpdatePara();
	afx_msg void OnBnClickedButtonTest1();
	afx_msg void OnBnClickedButtonTest2();
	afx_msg void OnBnClickedButtonTest3();
	afx_msg void OnBnClickedButtonTest4();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnClickedCheckDisp();
	afx_msg void OnClickedCheckAutoTuning();
	afx_msg void OnClickedRadio();
};

// Comm.cpp: implementation of the CComm class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Comm.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CComm::CComm()
{
	idComDev = NULL;
}

CComm::~CComm()
{
	Close();
}

BOOL CComm::Open(int nPort, DWORD BaudRate, BYTE ByteSize, BYTE Parity, BYTE StopBits)
{
	char szPort[128];

//	sprintf(szPort,"COM%d",nPort);
//	sprintf(szPort,"\\.\\COM%d",nPort);
	sprintf(szPort,"\\\\?\\COM%d",nPort);

	return Open(szPort, BaudRate, ByteSize, Parity, StopBits);
}

BOOL CComm::Open(char* szPort, DWORD BaudRate, BYTE ByteSize, BYTE Parity, BYTE StopBits)
{
	DCB dcb;
	COMMTIMEOUTS CommTimeOuts;

	if((idComDev=CreateFile(szPort, GENERIC_READ|GENERIC_WRITE,
		1,      // exclusive access
		NULL,   // no security attr
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,
		NULL))==(HANDLE)-1)
	{
		idComDev = NULL;
		return FALSE;
	} 

	SetCommMask(idComDev, EV_RXCHAR);
	SetupComm(idComDev, 4096, 4096);
	PurgeComm(idComDev, PURGE_TXABORT|PURGE_RXABORT|
					PURGE_TXCLEAR|PURGE_RXCLEAR);
	CommTimeOuts.ReadIntervalTimeout = 0xFFFFFFFF;
	CommTimeOuts.ReadTotalTimeoutMultiplier = 0;
	CommTimeOuts.ReadTotalTimeoutConstant = 1000;
	CommTimeOuts.WriteTotalTimeoutMultiplier = 0;
	CommTimeOuts.WriteTotalTimeoutConstant = 1000;
	SetCommTimeouts(idComDev, &CommTimeOuts);


	dcb.DCBlength = sizeof(DCB);
	GetCommState(idComDev, &dcb);
	dcb.BaudRate = BaudRate;
	dcb.ByteSize = ByteSize;
	dcb.Parity   = Parity;
	dcb.StopBits = StopBits;

	dcb.fOutxDsrFlow =0 ;//Dsr Flow
	dcb.fDtrControl = DTR_CONTROL_ENABLE ;//Dtr Control
	dcb.fOutxCtsFlow = 0 ;//Cts Flow
	dcb.fRtsControl = RTS_CONTROL_ENABLE ; //Ctr Control
	dcb.fInX = dcb.fOutX = 0 ; //XON/XOFF ���Ѱ�
//	dcb.XonChar = ASCII_XON ;
//	dcb.XoffChar = ASCII_XOFF ;
	dcb.XonLim = 100 ;
	dcb.XoffLim = 100 ;
	dcb.fBinary = TRUE ;
	dcb.fParity = TRUE ;
	dcb.fBinary = TRUE ;
	dcb.fParity = TRUE ;

	if(SetCommState( idComDev, &dcb )==FALSE)
	{
		CloseHandle(idComDev);
		return FALSE;
	}


	osWrite.Offset=0;
	osWrite.OffsetHigh=0;
	osRead.Offset=0;
	osRead.OffsetHigh=0;

	osRead.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	if(osRead.hEvent == NULL)
	{
		AfxMessageBox("Can not Create Event");
		return FALSE;
	}

	osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	if(osWrite.hEvent == NULL)
	{
		CloseHandle(osRead.hEvent);
		AfxMessageBox("Can not Create Event");
		return FALSE;
	}

	return TRUE;
}

void CComm::Close(void)
{
	if (idComDev == NULL)
		return;

    SetCommMask(idComDev,0);
    EscapeCommFunction(idComDev, CLRDTR);
    PurgeComm(idComDev, PURGE_TXABORT|PURGE_RXABORT|PURGE_TXCLEAR|PURGE_RXCLEAR);

    CloseHandle(idComDev);
    CloseHandle(osRead.hEvent);
    CloseHandle(osWrite.hEvent);
    
    idComDev = NULL;
}

int CComm::Read(LPSTR lpszBlock, DWORD dwMaxLength )
{
	BOOL       fReadStat ;
	COMSTAT    ComStat;
	DWORD      dwErrorFlags;
	DWORD      dwLength;

	// only try to read number of bytes in queue 
	ClearCommError( idComDev, &dwErrorFlags, &ComStat ) ;
	dwLength = min( dwMaxLength, ComStat.cbInQue ) ;

	if (dwLength > 0)
	{
		fReadStat = ReadFile( idComDev, lpszBlock,
		                    dwLength, &dwLength, &osRead ) ;
		if (!fReadStat)
		{
		}
   }

   return ( dwLength ) ;

} 

int CComm::Write(LPSTR lpByte, DWORD dwLength)
{
    BOOL fWriteState;
    DWORD dwBytesWritten;

    fWriteState=WriteFile(idComDev, lpByte, dwLength,&dwBytesWritten,&osWrite);

	COMSTAT    ComStat;
	DWORD      dwErrorFlags;
	ClearCommError( idComDev, &dwErrorFlags, &ComStat ) ;

    if(!fWriteState)
	{
		return dwBytesWritten;
    }

    return dwBytesWritten;
}

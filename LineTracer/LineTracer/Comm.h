// Comm.h: interface for the CComm class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMM_H__EBA0BA62_9595_11D1_8FDF_0000E81A45F9__INCLUDED_)
#define AFX_COMM_H__EBA0BA62_9595_11D1_8FDF_0000E81A45F9__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CComm  
{
public:
	CComm();
	virtual ~CComm();

	BOOL Open(int nPort, DWORD BaudRate, BYTE ByteSize=8, BYTE Parity=0, BYTE StopBits=0);
	BOOL Open(char* DevName, DWORD BaudRate, BYTE ByteSize=8, BYTE Parity=0, BYTE StopBits=0);
	void Close(void);
	int  Read(LPSTR, DWORD );
	int  Write(LPSTR, DWORD);
	int  IsConnected() {return (idComDev) ? 1:0;};

	OVERLAPPED osWrite,osRead;
	HANDLE idComDev;
};

#endif // !defined(AFX_COMM_H__EBA0BA62_9595_11D1_8FDF_0000E81A45F9__INCLUDED_)

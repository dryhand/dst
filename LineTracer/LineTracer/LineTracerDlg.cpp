
// LineTracerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LineTracer.h"
#include "LineTracerDlg.h"
#include "afxdialogex.h"
#include "opencv2/opencv.hpp"  

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#pragma comment(lib, "opencv_core2410D.lib")
#pragma comment(lib, "opencv_highgui2410D.lib")
#pragma comment(lib, "opencv_imgproc2410D.lib")

#define CONFIG_FILE		".\\config.ini"

#define NUM_PTS			10

#define B_LEFT			0		// Branch Left
#define B_CENTER		1		// Branch Center
#define B_RIGHT			2		// Branch Right

#define ID_STOP			8
#define ID_TURN_BACK	9
#define ID_TURN_LEFT	10
#define ID_TURN_RIGHT	11

#define PWM_MAX			2000
#define PWM_MANUAL		2100
#define PWM_NORMAL		2050
#define PWM_SLOW		1970
#define PWM_TURN		1970
#define PWM_RANGE		200

#define TURN_90			90		// 90도 회전
#define TURN_180		180		// 180도 회전

int g_RefR, g_RefG, g_RefB;
int g_Gain;
int g_Threshold;
int g_WidthMin,g_WidthMax;
#ifdef SEARCH_LINE
int g_DirSearch;		// SearchLine의 방향
#endif

int SaveValue(char *File, LPCTSTR Section, LPCTSTR Key, int Value)
{
    char value[256];
    
    sprintf(value, "%d", Value); 
    return WritePrivateProfileString(Section, Key, value, File);
}
 
int LoadValue(char *File, LPCTSTR Section, LPCTSTR Key, int *Value, int Default=0)
{
    char value[256];
    
    if (!GetPrivateProfileString (Section, Key, "", value, sizeof(value), File))
    {
        *Value = Default;
        return 0;
    }
    
    *Value = strtol(value, 0, 0);
    return 1;
}

double GetPassTime()
{
	const int TIME_BUF=10;

	static int t_count=0;
	static double t, t_buf[TIME_BUF];
	double t_sum,t_avg;

	t_buf[t_count%TIME_BUF] = ((double)getTickCount() - t)/getTickFrequency(); 
	t = (double)getTickCount(); 

	t_count++;
	t_sum=0;
	for (int t_i=0; t_i<TIME_BUF; t_i++)
		t_sum += t_buf[t_i];
	t_avg = t_sum/TIME_BUF;

	return t_avg*1000;
}

void onMouse( int evt, int x, int y, int flags, void* param )
{
	Mat image = *(Mat *)param;
	if (image.empty())
		return;

    if( evt == CV_EVENT_LBUTTONDOWN )
    {
		Point3_<uchar>* p = image.ptr<Point3_<uchar> >(y,x);

		g_RefB = p->x; //B
		g_RefG = p->y; //G
		g_RefR = p->z; //R
		cout << "R=" << g_RefR << ", G=" << g_RefG << ", B=" << g_RefB << endl;

		SaveValue(CONFIG_FILE, "Para", "RefR", g_RefR);
		SaveValue(CONFIG_FILE, "Para", "RefG", g_RefG);
		SaveValue(CONFIG_FILE, "Para", "RefB", g_RefB);
    }
}

// 이미지에 있는 라인의 중심점을 찾는다. 
// 라인은 검은색이다. (Threshold보다 값이 작다)
// 이미지 데이터 Mat Line은 1라인의 column matrix 혹은 row matrix이다. 
int GetLineCenter(Mat Line, int WidthMin, int WidthMax, int Threshold, int *Center, int *Width)
{
	BYTE *pixel = (BYTE *)Line.data;
	int len = max(Line.cols, Line.rows);

	int value, pre_value=Threshold;
	int sum=0, count=0;

	for (int i=0; i<len; i++)
	{
		value = pixel[i];

		if (value < Threshold && !(pre_value<Threshold))
		{
			sum = 0;
			count =0;
		}

		if (value < Threshold)
		{
			sum += i;
			count++;
		}

		if ( !(value < Threshold) && pre_value<Threshold)
		{
			if (WidthMin<count && count<WidthMax)	// count 크기가 맞으면
			{
				*Center = sum/count;
				*Width = count;

				return 1;
			}
		}

		pre_value = value;
	}

	if (WidthMin<count && count<WidthMax)	// count 크기가 맞으면
	{
		*Center = sum/count;
		*Width = count;
		return 1;
	}
	else
	{
		*Center = 0;
		*Width = 0;
	}

	return 0;
}

void CvtGrey(Mat& ImgIn, Mat& ImgOut)
{
	if (ImgOut.empty())
		ImgOut.create(ImgIn.size(), CV_8UC1);

	BYTE *in = (BYTE *)ImgIn.data;
	BYTE *out = (BYTE *)ImgOut.data;

	for (int i=0; i<ImgIn.total(); i++)
	{
		int diff_b, diff_g, diff_r;
		diff_b = abs(in[3*i]-g_RefB);
		diff_g = abs(in[3*i+1]-g_RefG);
		diff_r = abs(in[3*i+2]-g_RefR);

		int sum = (diff_b+diff_g+diff_r)*g_Gain/100;
		sum = min(255, sum);

		out[i] = sum;
	}
}

/*
	X1 = aY1 + b
	X2 = aY2 + b

	a = (X1-X2)/(Y1-Y2)
	b = X1 - aY1
*/
// PtX, PtY를 이용하여 라인을 찾는다.
// 찾은 라인의 A,B (x=Ay+B)를 리턴한다.
int FitLine(int *PtX, int *PtY, int PtCount, double *A, double *B)
{
	double a,b;
	int n1, n2, count;

	for (n1=0; n1<PtCount-2; n1++)
		for (n2=PtCount-1; n2>1; n2--)
		{
			a = (double)(PtX[n1]-PtX[n2])/(PtY[n1]-PtY[n2]);
			b = PtX[n1] - a*PtY[n1];

			count = 0;
			for (int i=0; i<PtCount; i++)
			{
				double dist = fabs(a*PtY[i] + b - PtX[i]);
				if (dist<4)
					count++;
//	printf("%d : %f\n", i, dist);
			}

			if (count > PtCount/2)
			{
				*A = a;
				*B = b;
				return 1;
			}
		}

	return 0;
}

// 수직선을 찾는다.
int FindPtsV(Mat Image, int *PtX, int *PtY, int *Width)
{
	int rtn;
	int width = Image.cols;
	int height = Image.rows;
	int step = height/NUM_PTS;
	int pt_count = 0;

	for (int y=step; y<height; y+=step)
	{
		int x, width;
		Mat line_gray;
		Mat line;
		Image.row(y).copyTo(line);

		CvtGrey(line, line_gray);
		rtn = GetLineCenter(line_gray, g_WidthMin, g_WidthMax, g_Threshold, &x, &width);

		if (rtn)
		{
			PtX[pt_count] = x;
			PtY[pt_count] = y;
			Width[pt_count] = width;
			pt_count++;
		}
	}

	return pt_count;
}

// 수평선을 찾는다.
int FindPtsH(Mat Image, int *PtX, int *PtY)
{
	int rtn;
	int width = Image.cols;
	int height = Image.rows;
	int step = width/NUM_PTS;
	int pt_count = 0;

	for (int x=step; x<width; x+=step)
	{
		int y, length;
		Mat line_gray;
		Mat line;
		Image.col(x).copyTo(line);

		CvtGrey(line, line_gray);
		rtn = GetLineCenter(line_gray, g_WidthMin, g_WidthMax, g_Threshold, &y, &length);

		if (rtn)
		{
			PtX[pt_count] = x;
			PtY[pt_count] = y;
			pt_count++;
		}
	}

	return pt_count;
}

// 이미지(ImgIn) 상에서 직선을 찾는다.
// 찾은 결과는 ImgOut에 출력된다.
int FindLine(Mat& ImgIn, Mat& ImgOut, int *LineWidth, double *A, double *B)
{
	int x1,y1,x2,y2;
	int pt_x[20], pt_y[20], line_w[20], line_width, pt_count;

	int width = ImgIn.cols;
	int height = ImgIn.rows;
	int step = height/NUM_PTS;
	int sum_width=0;
	double a, b;
			
	pt_count = FindPtsV(ImgIn, pt_x, pt_y, line_w);
	if (pt_count>=4 && FitLine(pt_x, pt_y, pt_count, &a, &b))
	{
		for (int i=0; i<pt_count; i++)
		{
			circle(ImgOut, Point(pt_x[i], pt_y[i]), 6, Scalar(0, 255,0), 3);
			sum_width += line_w[i];
		}
		line_width = sum_width/pt_count;

		*LineWidth = line_width;
		*A = a;
		*B = b;

		y1 = 0;
		x1 = (int)(a*y1 + b);
		y2 = (int)(height-1);
		x2 = (int)(a*y2 + b);
		line(ImgOut, Point(x1,y1), Point(x2,y2), Scalar(255, 255,0), 3);

#ifdef SEARCH_LINE
		int x = (x1+x2)/2;
		if (x<CAM_WIDTH/2)
			g_DirSearch = 1;
		else
			g_DirSearch = -1;
#endif
		return pt_count;
	}

	return 0;
}

// 브랜치를 찾는다.
int CheckBranch(Mat& ImgIn, Mat& ImgOut,int Width, double A, double B, int LR)
{
	int rtn=0;
	int x,y;
	int sign=1;

	if (LR==B_LEFT)
		sign = -1;
	if (LR==B_RIGHT)
		sign = 1;
	
	double b = B+sign*sqrt(A*A+1)*Width;

	Mat line, line_gray;
	ImgIn.col(0).copyTo(line);

	int height = ImgIn.rows;
	int width = ImgIn.cols;
	for (int y=0; y<height; y++)
	{
		Vec3b aa;
		int x = (int)(A*y + b);
		x = max(0,x);
		x = min(x,width-1);
		line.at<Vec3b>(y,0) = ImgIn.at<Vec3b>(y,x);
	}

	CvtGrey(line, line_gray);
	int length;
	rtn = GetLineCenter(line_gray, g_WidthMin, g_WidthMax, g_Threshold, &y, &length);

	if (rtn)
	{
		x = (int)(A*y + b);
		circle(ImgOut, Point(x,y), 6, Scalar(255, 255,255), 6);
	}

	return rtn;
}

// 바코드를 읽어서 ID 값을 가져온다.
int CheckID(Mat& ImgIn, Mat& ImgOut, int Width, double A, double B)
{
	const int NUM_BITS=4;
	const int num_id = 1<<NUM_BITS;
	int width = ImgIn.cols;
	int height = ImgIn.rows;
	int step = height/20;
	int id=0, count=0;
	int ID[num_id];

	memset(ID, 0, sizeof(ID));
	for (int y=step; y<height; y+=step)
	{
		Mat line_gray;
		Mat line;
		ImgIn.row(y).copyTo(line);
		CvtGrey(line, line_gray);
		BYTE *pixel = (BYTE *)line_gray.data;

		int threshold = g_Threshold;
		int x = (int)(A*y + B);
		int offset = Width*8/10;
		int start = max(x-offset, 1);
		int end = min(x+offset, width);

		int start_x, len_black[16], len_white[16];
		int c_black=0, c_white=0;

		for (x=start; pixel[x]>threshold && x<end; x++);	// white
		//circle(ImgOut, Point(x, y), 1, Scalar(0, 255, 0), 3);

		for (; x<end; x++)
		{
			start_x = x;
			for (x+=2; pixel[x]<=threshold && x<end; x++);	// black
			if(x!=end)
			{
				len_black[c_black++] = x-start_x;
				//circle(ImgOut, Point(x, y), 1, Scalar(0, 0, 255), 3);
			}

			start_x = x;
			for (x+=2; pixel[x]>threshold && x<end; x++);	// white
			if(x!=end)
			{
				len_white[c_white++] = x-start_x;
				//circle(ImgOut, Point(x, y), 1, Scalar(0, 255, 0), 3);
			}
		}

		id = 0;
		if (c_black == NUM_BITS+1 && c_white == NUM_BITS)
		{
			// 첫번째 비트의 크기를 보고 순/역방향을 정한다.
			if (len_black[0]>len_black[c_black-1])
			{
				// 순방향
				for (int i=0; i<c_black-1; i++)
					if (len_black[i]>len_white[i])
						id = (id<<1) | 1;
					else
						id = (id<<1) | 0;
				ID[id]++;
			}
			else
			{
				// 역방향
				for (int i=c_black-1; i>0; i--)
					if (len_black[i]>len_white[i-1])
						id = (id<<1) | 1;
					else
						id = (id<<1) | 0;
				ID[id]--;
			}
		}
	}

	static int a;
	for (int i=0; i<num_id; i++)
	{
		if (ID[i]>=2)
		{
			putText(ImgOut, "ID : " + to_string((_Longlong)i), Point(10, 40), FONT_HERSHEY_PLAIN, 2, Scalar(255, 0, 0),3);
//			printf("ID = %d %d\n", i, a++);
			return i;
		}
		if (ID[i]<=-2)
		{
			putText(ImgOut, "ID : " + to_string((_Longlong)-i), Point(10, 40), FONT_HERSHEY_PLAIN, 2, Scalar(255, 0, 0),3);
//			printf("ID = %d %d\n", i, a++);
			return i;
		}
	}

	return 0;
}

void thread_ctrl(CLineTracerDlg* ptr)
{
	ptr->ThreadCtrl();
}

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CLineTracerDlg dialog
CLineTracerDlg::CLineTracerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLineTracerDlg::IDD, pParent)
	, m_Branch(1)
	, m_bAutoTuning(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLineTracerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_RADIO_L, m_Branch);
	DDX_Check(pDX, IDC_CHECK_AUTO_TUNING, m_bAutoTuning);
	DDX_Control(pDX, IDC_SLIDER_FB, m_scFB);
	DDX_Control(pDX, IDC_SLIDER_LR, m_scLR);
}

BEGIN_MESSAGE_MAP(CLineTracerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_START, &CLineTracerDlg::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_STOP, &CLineTracerDlg::OnBnClickedButtonStop)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDOK, &CLineTracerDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_UPDATE_PARA, &CLineTracerDlg::OnBnClickedButtonUpdatePara)
	ON_BN_CLICKED(IDC_RADIO_L, &CLineTracerDlg::OnClickedRadio)
	ON_BN_CLICKED(IDC_RADIO_C, &CLineTracerDlg::OnClickedRadio)
	ON_BN_CLICKED(IDC_RADIO_R, &CLineTracerDlg::OnClickedRadio)
	ON_WM_TIMER()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_BN_CLICKED(IDC_BUTTON_TEST1, &CLineTracerDlg::OnBnClickedButtonTest1)
	ON_BN_CLICKED(IDC_BUTTON_TEST2, &CLineTracerDlg::OnBnClickedButtonTest2)
	ON_BN_CLICKED(IDC_BUTTON_TEST3, &CLineTracerDlg::OnBnClickedButtonTest3)
	ON_BN_CLICKED(IDC_BUTTON_TEST4, &CLineTracerDlg::OnBnClickedButtonTest4)
	ON_BN_CLICKED(IDC_CHECK_DISP, &CLineTracerDlg::OnClickedCheckDisp)
	ON_BN_CLICKED(IDC_CHECK_AUTO_TUNING, &CLineTracerDlg::OnClickedCheckAutoTuning)
END_MESSAGE_MAP()


// CLineTracerDlg message handlers

BOOL CLineTracerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	AllocConsole();
	freopen("CONOUT$", "wt", stdout);
	OnBnClickedButtonUpdatePara();

	m_bCheck = FALSE;

	int com;
	LoadValue(CONFIG_FILE, "Ctrl", "COM", &com, 3);
	if (m_Comm.Open(com, 115200))
		printf("Open COM%d\n", com);
	else
		printf("Can't Open COM%d\n", com);

	Sleep(500);	// Sleep()이 없으면 카메라가 가끔 동작하지 않는다.
	m_pThreadCtrl = AfxBeginThread((AFX_THREADPROC)thread_ctrl, this);	// start thread

	namedWindow("ImgOut", 1);  

    // event handler 등록
    setMouseCallback( "ImgOut", onMouse, &m_ImgIn );

	m_scFB.SetRange(-PWM_RANGE, PWM_RANGE, TRUE);
	m_scLR.SetRange(-PWM_RANGE, PWM_RANGE, TRUE);
	m_scFB.SetPos(0);
	m_scLR.SetPos(0);

	m_PWM_L = 0;
	m_PWM_R = 0;
	m_Man_L = 0;
	m_Man_R = 0;
	m_Vel_L = 0;
	m_Vel_R = 0;

	m_Mode = M_ManualControl;
	m_TurnCount = 0;

	// 수동제어로 변경되면 컨트롤보드의 LED가 on됨
	SetTorque(0);
	ChangeMode(1);	// 0:속도제어, 1:수동제어(PWM)
	SetTorque(1);

	m_PWM_Max = PWM_MAX;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLineTracerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLineTracerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLineTracerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CLineTracerDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: Add your message handler code here
	if (m_pThreadCtrl)
	{
		m_bThreadCtrl = false;

		DWORD status;
		while (GetExitCodeThread(m_pThreadCtrl->m_hThread, &status))
			Sleep(10);
	}

	FreeConsole();
}

void CLineTracerDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here

	CDialogEx::OnOK();
}

void CLineTracerDlg::OnBnClickedButtonStart()
{
	// TODO: Add your control notification handler code here
	m_Mode = M_TrackingNormal;
}

void CLineTracerDlg::OnBnClickedButtonStop()
{
	// TODO: Add your control notification handler code here
	m_Mode = M_ManualControl;

//	SendPacketRun(0, 0);
//	SendPacketOut(0, 0);
}

void CLineTracerDlg::OnBnClickedButtonUpdatePara()
{
	// TODO: Add your control notification handler code here
	LoadValue(CONFIG_FILE, "Para", "RefR", &g_RefR, g_RefR);
	LoadValue(CONFIG_FILE, "Para", "RefG", &g_RefG, g_RefG);
	LoadValue(CONFIG_FILE, "Para", "RefB", &g_RefB, g_RefB);
	LoadValue(CONFIG_FILE, "Para", "Gain", &g_Gain, g_Gain);
	LoadValue(CONFIG_FILE, "Para", "Threshold", &g_Threshold, g_Threshold);
	LoadValue(CONFIG_FILE, "Para", "WidthMin", &g_WidthMin, g_WidthMin);
	LoadValue(CONFIG_FILE, "Para", "WidthMax", &g_WidthMax, g_WidthMax);

	CString str, temp;
	temp.Format("RGB=(%3d,%3d,%3d)\r\n", g_RefR, g_RefG, g_RefB); str+=temp;
	temp.Format("Gain=%d\r\n", g_Gain); str+=temp;
	temp.Format("Threshold=%d\r\n", g_Threshold); str+=temp;
	temp.Format("WidthMin=%d\r\n", g_WidthMin); str+=temp;
	temp.Format("WidthMax=%d\r\n", g_WidthMax); str+=temp;
	temp.Format("\r\n"); str+=temp;
		
	SetDlgItemText(IDC_EDIT_PARA, str);
}

void CLineTracerDlg::OnClickedCheckDisp()
{
	// TODO: Add your control notification handler code here
	m_bCheck = IsDlgButtonChecked(IDC_CHECK_DISP);

	if (m_bCheck)
	{
		namedWindow("img_gray", 1);  
		namedWindow("img_threshold", 1);  
	}
	else
	{
		destroyWindow("img_gray");  
		destroyWindow("img_threshold");  
	}
}

void CLineTracerDlg::SendPacketRun(int Left, int Right)
{
	BYTE packet[15] = {0xff, 0xff, 0xee, 0x0b, 0x24, 0x02, 0x01, 0x01, 0x03, 0xe8, 0x02, 0x01, 0x03, 0xe8, 0xfa};
	BYTE cs=0;

	if (Left>0)
		packet[7] = 1;
	else
		packet[7] = 2;

	Left = abs(Left);
	packet[8] = (Left>>8)&0xff;
	packet[9] = (Left)&0xff;

	if (Right>0)
		packet[11] = 2;
	else
		packet[11] = 1;

	Right = abs(Right);
	packet[12] = (Right>>8)&0xff;
	packet[13] = (Right)&0xff;

	for (int i=2; i<14; i++)
		cs += packet[i];
	cs &= 0xfe;

	packet[14] = cs;

	m_Comm.Write((char *)packet, sizeof(packet));
}

void CLineTracerDlg::SendPacketOut(int Left, int Right)
{
	BYTE packet[] = {0xff, 0xff, 0xee, 0x0d, 0x26, 0x02, 0x01, 0x01, 0x05, 0x07, 0xd0, 0x02, 0x02, 0x05, 0x07, 0xd0, 0xe0};
	BYTE cs=0;

	if (Left>0)
		packet[7] = 1;
	else
		packet[7] = 2;

	Left = abs(Left);
	packet[9] = (Left>>8)&0xff;
	packet[10] = (Left)&0xff;

	if (Right>0)
		packet[12] = 2;
	else
		packet[12] = 1;

	Right = abs(Right);
	packet[14] = (Right>>8)&0xff;
	packet[15] = (Right)&0xff;

	for (int i=2; i<16; i++)
		cs += packet[i];
	cs &= 0xfe;

	packet[16] = cs;

	m_Comm.Write((char *)packet, sizeof(packet));
}

// 0 : 속도 제어
// 1 : 수동 제어
void CLineTracerDlg::ChangeMode(int Mode)
{
	BYTE packet[] = {0xff, 0xff, 0x01, 0x03, 0x42, 0x01, 0x46};
	BYTE cs;

	cs = 0;
	packet[2] = 0x01;
	packet[5] = Mode;
	for (int i=2; i<6; i++)
		cs += packet[i];
	cs &= 0xfe;
	packet[6] = cs;
	m_Comm.Write((char *)packet, sizeof(packet));

	cs = 0;
	packet[2] = 0x02;
	packet[5] = Mode;
	for (int i=2; i<6; i++)
		cs += packet[i];
	cs &= 0xfe;
	packet[6] = cs;
	m_Comm.Write((char *)packet, sizeof(packet));
}

void CLineTracerDlg::SetTorque(int OnOff)
{
	BYTE packet_off[] = {0xff, 0xff, 0xee, 0x07, 0x22, 0x02, 0x01, 0x00, 0x02, 0x00, 0x1c};	// torque off
	BYTE packet_on[] = {0xff, 0xff, 0xee, 0x07, 0x22, 0x02, 0x01, 0x01, 0x02, 0x01, 0x1e};	// torque on

	if (OnOff)
		m_Comm.Write((char *)packet_on, sizeof(packet_on));
	else
		m_Comm.Write((char *)packet_off, sizeof(packet_off));
}

void CLineTracerDlg::GetVelocity(int *Left, int *Right)
{
	const int t_sleep=5;	// 5ms 정도 되어야 안정적이다.
	int len;
	char packet[]= {0xFF, 0xFF, 0x01, 0x03, 0x01, 0x01, 0x06};
	BYTE data[36];

	m_Comm.Write(packet, 7);

	Sleep(t_sleep);
	len = m_Comm.Read((char *)data, sizeof(data));

	if (len==13)
	{
		//printf("len = %d\n", len);
		//for (int i=0; i<len; i++)
		//	printf("%02x ", data[i] & 0xff);
		//printf("\n\n", len);

		*Left = ((int)data[8]<<8)|data[9];
	}

	packet[2] = 0x02;
	m_Comm.Write(packet, 7);

	Sleep(t_sleep);
	len = m_Comm.Read((char *)data, sizeof(data));

	if (len==13)
	{
		//printf("len = %d\n", len);
		//for (int i=0; i<len; i++)
		//	printf("%02x ", data[i] & 0xff);
		//printf("\n\n", len);

		*Right = ((int)data[8]<<8)|data[9];
	}
}

void CLineTracerDlg::OnBnClickedButtonTest1()
{
	// TODO: Add your control notification handler code here
	// 수동제어로 변경되면 컨트롤보드의 LED가 on됨
	SetTorque(0);
	ChangeMode(1);	// 0:속도제어, 1:수동제어
	SetTorque(1);
}


void CLineTracerDlg::OnBnClickedButtonTest2()
{
	// TODO: Add your control notification handler code here
	//BYTE packet[] = {0xff,0xff,0xee,0x0b,0x24,0x02,0x01,0x01,0x03,0xe8,0x02,0x02,0x03,0xe8,0xfa};

	//m_Comm.Write((char *)packet, sizeof(packet));
//	SendPacketRun(-1000, 1000);
	SendPacketOut(-PWM_MANUAL, PWM_MANUAL);
}


void CLineTracerDlg::OnBnClickedButtonTest3()
{
	// TODO: Add your control notification handler code here
//	SendPacketRun(1000, -1000);
	SendPacketOut(PWM_MANUAL, -PWM_MANUAL);
}


void CLineTracerDlg::OnBnClickedButtonTest4()
{
	// TODO: Add your control notification handler code here
	SendPacketOut(0, 0);
}

int CLineTracerDlg::TurnRobot(int Count, int *PWM_L, int *PWM_R)
{
	if (m_TurnCount>=abs(Count))
	{
		m_TurnCount = 0;
		*PWM_L = 0;
		*PWM_R = 0;
	}
	else
	{
		m_TurnCount++;
		
		if (Count>0)
		{
			*PWM_L = PWM_TURN;
			*PWM_R = -PWM_TURN;
		}
		else
		{
			*PWM_L = -PWM_TURN;
			*PWM_R = PWM_TURN;
		}
	}

//printf("Count = %d\n", m_TurnCount);
	return m_TurnCount;
}
/*
int CLineTracerDlg::TurnRobot(int Count, int *PWM_L, int *PWM_R)
{
	// 로봇이 움직이고 있으면 멈춘후 동작한다. 
	if (m_TurnCount==0)
		if (m_Vel_L!=0 || m_Vel_R!=0)
		{
			*PWM_L = 0;
			*PWM_R = 0;

printf("Vel = (%d,%d)\n", m_Vel_L, m_Vel_R);
			return -1;
		}

	if (m_TurnCount>=abs(Count))
	{
		m_TurnCount = 0;
		*PWM_L = 0;
		*PWM_R = 0;
	}
	else
	{
		m_TurnCount++;
		
		if (Count>0)
		{
			*PWM_L = PWM_TURN;
			*PWM_R = -PWM_TURN;
		}
		else
		{
			*PWM_L = -PWM_TURN;
			*PWM_R = PWM_TURN;
		}
	}

printf("Count = %d\n", m_TurnCount);
	return m_TurnCount;
}
*/
void CLineTracerDlg::ControlRobot(int RefPWM, double A, double B, int *PWM_L, int *PWM_R)
{
	int ref_y = CAM_HEIGHT*7/10;
	int ref_x = (int)(A*ref_y + B);

	int limit=50;
	double gain_position = 0.3;
	double gain_angle = 6;

	double err_position = CAM_WIDTH/2 - ref_x;
	double err_angle = atan(A)*180/3.14159; 
	double out = err_position*gain_position+err_angle*gain_angle;

	int pwm_l = (int)(RefPWM-out);
	int pwm_r = (int)(RefPWM+out);
	*PWM_L = min(pwm_l, RefPWM+limit);
	*PWM_R = min(pwm_r, RefPWM+limit);

	int pwm = (*PWM_L + *PWM_R)/2;
	int vel = (m_Vel_L + m_Vel_R)/2;
	if (pwm)
	{
		m_PWM_Avg = GetPWMAvg(pwm);
		m_Vel_Avg = GetVelAvg(vel);

		printf("PWM=%4d, Vel=%4d\n", m_PWM_Avg, m_Vel_Avg);
	}

	//circle(m_ImgTemp, Point(ref_x,ref_y), 6, Scalar(0, 255,255), 6);

	//int robot_y = ref_y;
	//int robot_x = CAM_WIDTH/2;
	//circle(m_ImgTemp, Point(robot_x,robot_y), 6, Scalar(0, 0,255), 6);

	//line(m_ImgTemp, Point(robot_x+30,robot_y), Point(robot_x+30,robot_y-right*0.03), Scalar(0,0,255), 5);
	//line(m_ImgTemp, Point(robot_x-30,robot_y), Point(robot_x-30,robot_y-left*0.03), Scalar(0,0,255), 5);
}

void CLineTracerDlg::ThreadCtrl(void)
{
	VideoCapture capture(0);	//설치되어 있는 웹캠 번호를 확인합니다.
	if (!capture.isOpened())  
	{  
		cout << "웹캠을 열수 없습니다." << endl;
		return ;
	}  
  
	SetTimer(1, 100, NULL);

	//캡처 영상 해상도 지정  
	capture.set(CV_CAP_PROP_FRAME_WIDTH, CAM_WIDTH);  
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, CAM_HEIGHT);  
  
	int rtn;

	m_bThreadCtrl = true;

	while(m_bThreadCtrl)  
	{  
		//웹캡으로부터 한 프레임을 읽어옴  
		capture >> m_ImgIn;  

		//화면에 영상을 보여줌
		if (!m_ImgIn.empty())
		{
			m_PassTime = GetPassTime();
			GetVelocity(&m_Vel_L, &m_Vel_R);
			m_ImgIn.copyTo(m_ImgTemp);

			int line=0, branch_l=0, branch_r=0, id=0;
			int line_width;
			double a, b;	// x=ay+b

			if (line=FindLine(m_ImgIn, m_ImgTemp, &line_width, &a, &b))
			{
				id = CheckID(m_ImgIn, m_ImgTemp, line_width, a, b);
				if (id==ID_STOP)
					m_Mode=M_ManualControl;
				else if (id==ID_TURN_BACK)
					m_Mode=M_TurnBack;

				branch_l = CheckBranch(m_ImgIn, m_ImgTemp, line_width, a, b, B_LEFT);
				branch_r = CheckBranch(m_ImgIn, m_ImgTemp, line_width, a, b, B_RIGHT);
			}
			else
			{
#ifdef SEARCH_LINE
				if (m_Mode == M_TrackingNormal || 
					m_Mode == M_TrackingSlow)
					m_Mode = M_SearchLine;
#else
				if (m_Mode == M_TrackingNormal || 
					m_Mode == M_TrackingSlow)
					m_Mode = M_ManualControl;
#endif
			}

			if (m_Mode == M_ManualControl)
			{
				m_PWM_L = m_Man_L;
				m_PWM_R = m_Man_R;
			} 
			else if (m_Mode == M_TurnLeft)
			{
				rtn = TurnRobot(-TURN_90, &m_PWM_L, &m_PWM_R);
				if (rtn==0)
					m_Mode = M_TrackingNormal;
			}
			else if (m_Mode == M_TurnRight)
			{
				rtn = TurnRobot(TURN_90, &m_PWM_L, &m_PWM_R);
				if (rtn==0)
					m_Mode = M_TrackingNormal;
			}
			else if (m_Mode == M_TurnBack)
			{
				rtn = TurnRobot(TURN_180, &m_PWM_L, &m_PWM_R);
				if (rtn==0)
					m_Mode = M_TrackingNormal;
			}
#ifdef SEARCH_LINE
			else if (m_Mode == M_SearchLine)
			{
				if (line)
				{
					m_Mode = M_TrackingNormal;
					m_TurnCount = 0;
				}
				else
				{
					rtn = TurnRobot(g_DirSearch*TURN_90, &m_PWM_L, &m_PWM_R);
					if (rtn==0)
						m_Mode = M_ManualControl;
				}
			}
#endif
			else if (m_Mode == M_TrackingNormal)
			{
				ControlRobot(PWM_NORMAL, a, b, &m_PWM_L, &m_PWM_R);

				if (id==ID_TURN_LEFT)
				{
					m_Branch = B_LEFT;
					m_Mode = M_TrackingSlow;
				}
				else if (id==ID_TURN_RIGHT)
				{
					m_Branch = B_RIGHT;
					m_Mode = M_TrackingSlow;
				}
			}
			else if (m_Mode == M_TrackingSlow)
			{
				ControlRobot(PWM_SLOW, a, b, &m_PWM_L, &m_PWM_R);

				//printf("Branch=(%d,%d)\n", branch_l,branch_r);
				if (m_Branch==B_LEFT && branch_l)
					m_Mode = M_TurnLeft;
				else if (m_Branch==B_RIGHT && branch_r)
					m_Mode = M_TurnRight;
			}

			m_ImgTemp.copyTo(m_ImgOut);
		}

		//if (m_PWM_L && m_PWM_R)
		//	printf("PWM=(%5d,%5d), Vel=(%5d,%5d)\n", m_PWM_L, m_PWM_R, m_Vel_L, m_Vel_R);
		SendPacketOut(m_PWM_L, m_PWM_R);

		waitKey(2);
	}  

	KillTimer(1);
}

void CLineTracerDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	Mat img_gray;  
	Mat img_threshold;  
	CString mode[] = {
		"ManualControl",
		"TurnLeft",
		"TurnRight",
		"TurnBack",
		"TrackingNormal",
		"TrackingSlow",
	};

	UpdateData(FALSE);

	if (!m_ImgIn.empty() && !m_ImgOut.empty())
	{
		if (m_bCheck)
		{
			CvtGrey(m_ImgIn, img_gray);
			threshold(img_gray, img_threshold, g_Threshold, 255, THRESH_BINARY);
		}

		/// Text
		string str = mode[m_Mode];
		cv::putText(m_ImgOut, str, Point(220,40), CV_FONT_HERSHEY_DUPLEX, 1.0, cv::Scalar(0,100,255), 2);
		imshow("ImgOut", m_ImgOut);

		if (m_bCheck)
		{
			imshow("img_gray", img_gray);
			imshow("img_threshold", img_threshold);
		}
	}

	CString str;
	str.Format("Time = %.1fms", m_PassTime);
	SetDlgItemText(IDC_STATIC_TIME, str);

	SetDlgItemInt(IDC_STATIC_PWM_L, m_PWM_L);
	SetDlgItemInt(IDC_STATIC_PWM_R, m_PWM_R);
	SetDlgItemInt(IDC_STATIC_VEL_L, m_Vel_L);
	SetDlgItemInt(IDC_STATIC_VEL_R, m_Vel_R);

	SetDlgItemText(IDC_STATIC_MODE, mode[m_Mode]);

	CDialogEx::OnTimer(nIDEvent);
}


void CLineTracerDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	//printf("nPos = %d\n", nPos);
	m_Mode = M_ManualControl;

	if (nSBCode==SB_ENDSCROLL)
		m_scLR.SetPos(0);

	int pos = m_scLR.GetPos();
	m_Man_L =  (PWM_MANUAL-(PWM_RANGE-abs(pos)));
	m_Man_R = -(PWM_MANUAL-(PWM_RANGE-abs(pos)));

	if (abs(pos)<20)
	{
		m_Man_L = 0;
		m_Man_R = 0;
	}
	else if (pos>0)
	{
		m_Man_L = -m_Man_L;
		m_Man_R = -m_Man_R;
	}

	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}


void CLineTracerDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	//printf("nPos = %d\n", nPos);
	m_Mode = M_ManualControl;

	if (nSBCode==SB_ENDSCROLL)
		m_scFB.SetPos(0);

	int pos = m_scFB.GetPos();
	m_Man_L = PWM_MANUAL-(PWM_RANGE-abs(pos));
	m_Man_R = PWM_MANUAL-(PWM_RANGE-abs(pos));

	if (abs(pos)<20)
	{
		m_Man_L = 0;
		m_Man_R = 0;
	}
	else if (pos>0)
	{
		m_Man_L = -m_Man_L;
		m_Man_R = -m_Man_R;
	}

	CDialogEx::OnVScroll(nSBCode, nPos, pScrollBar);
}


void CLineTracerDlg::OnClickedRadio()
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	printf("Branch = %d\n", m_Branch);
}


void CLineTracerDlg::OnClickedCheckAutoTuning()
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	double gain = 1.0;

	printf("AutoTuning = %d\n", m_bAutoTuning);
	if (m_bAutoTuning)
	{
//		m_PWM_Max += (int)(m_PWM_Avg-m_Vel_Avg*gain);
	}
}

int CLineTracerDlg::GetPWMAvg(int PWM)
{
	const int MAX_BUF=100;

	static int count=0;
	static int t, buf[MAX_BUF];
	int sum,avg;

	buf[count%MAX_BUF] = PWM; 
	count++;

	sum=0;
	for (int i=0; i<MAX_BUF; i++)
		sum += buf[i];
	avg = sum/MAX_BUF;

	return avg;
}

int CLineTracerDlg::GetVelAvg(int Vel)
{
	const int MAX_BUF=100;

	static int count=0;
	static int t, buf[MAX_BUF];
	int sum,avg;

	buf[count%MAX_BUF] = Vel; 
	count++;

	sum=0;
	for (int i=0; i<MAX_BUF; i++)
		sum += buf[i];
	avg = sum/MAX_BUF;

	return avg;
}


